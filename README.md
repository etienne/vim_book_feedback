# Vim book - feedback

This readme file goes through the book chapters in order, whenever some feedback
is given.

## Start

> Install the plugins and update your configuration

A beginner would not know about the existence of vim plugins. How about
introducing as examples to the previous sentence

```diff
- Feel free to takes pauses and try out what you learn. Install the plugins and update your configuration
+ Feel free to takes pauses and try out what you learn (eg. install plugins, update your configuration, etc)
```

## Neovim & Installation

> Additionally, you should install a patched powerline font or a nerd font and configure your terminal

The fonts here might be used in the vim status bar, I suppose. To minimize the
number of dependencies to install upfront (which may be preferred by a beginner), consider suggesting a status plugin
that does not require any font and that's still nice, such as lightline (https://github.com/itchyny/lightline.vim)

## Modal

I like the summary table at the end of this section!

## Editing and Exiting

So at this point (or maybe in a different additional section), how about
introducing `vimtutor`?

Years ago, I went through `vimtutor` multiple times over a few weeks. I thought
that repeating this exercise was an great way to build muscle-memory for the
basic moves.

I'm aware that `vimtutor` content may overlap with yours, but again, repetition
is key (for beginners at least), so it would not hurt.

### Basic Editing without any vimisms

> In VS Code, we can simply use the mouse to double click the word and delete it. This will all be easier once we learn to dance with vim.

Maybe keep driving your point home: how about explicitly saying that the whole movement restrictions (ie. no mouse) will enable the developer not to be slowed down? That may sound a bit paradoxal, so I think it's worthwhile writing a couple of sentence about it.

### Insert mode

```diff
- I - Enters insert mode at the start of the line.
+ I - Enters insert mode at the start of the line, or at indentation level.
```

How about mentioning `C`:

```
C - Insert mode at current position, deletes everything else that follows on the
same line.
```

### Vim Configuration

```diff
- Let’s make set number permanent.
+ Let's make `:set number` permamanent by creating our first Vim configuration file.
```

### Some Starting Configuration

How about adding inline comments in the config file, eg.

```
vim.opt.number = true -- enables line numbers, if set to true
```

If you're not doing that, maybe you could suggest doing it. Maybe a habit for
the new developer to pick up: add inline comments whenever there's a
documentation added-value, as a new config line is written.

## Plugins

### Install lazy.nvim

You code snippets don't always have the file name that it's related to. How
about consistently indicating where we find ourselves (first line of the code
snippet) eg.?

```
-- in ~/.config/nvin/init.lua

...
```

> This code checks to see if the lazy.nvim plugin is installed. If not, it will be cloned and installed

Where will it be installed?

> As you copy/paste the plugin’s code, you’ll note that it’s not indented properly. While in normal mode, on each line press == to indent the line properly.

Maybe it's an opportunity to talk about the `:set paste` mode? (then mention
`:set nopaste`)?

## More Basic Configuration

What is `vim.opt.list` about? You didn't give any description about it.

### Folding

I find that folding can be a bit finicky, so I know you can use the visual line mode
(`V`) to select what you want to fold (in manual `foldmethod`, at least). But at the same time, I never really
spent time to have a strong fold config, so this tip is what it is :)

## Text navigation

### Motion Plugin Hop

```
-- 'phaazon/hop.nvim',
   'thisduck/hop.nvim',
```

How about adding a comment inline (or a short sentence under the code snippet), explaining why you recommending using your fork, as opposed to using the original plugin?

The paragraph that starts with this sentence:

> Typically a lot of shortcuts in programs start with the control key...

I think it should be its own "Leader key" section, and not be part of the "Motion Plugin Hop"
section. But since you go back then to the hop plugin, to illustrate the
configuration of the leader key, I'm not sure how these two sets of information
should be arranged.

How about having a "Leader key" sub-section in the "More Basic Configuration"
section above?

## More Editing

### Action and Motion

After introducing `d`, what do you think about also introducing the put/paste command
(`p`, `P`)? I think they often go hand in hand.

> dd will delete the current line

Technically, all the `d` operations do more than deleting. They also store value
in a register, which make the use of a put command possible.

## Visual Mode

The "A little cheat" appears as a sub-section of "Visual Mode" in the table of
contents at the very top, while it's not. It's more like a separate section that
happens to be here.

> I don’t really use <Esc>. Instead I use <C-c>

I use `jk` and `kj` as a remap of <Esc>, since my right hand rests on these keys
by default: these keys are used for movements but not in insert mode which is
why I'm using them as a way to leave insert mode. Just a suggestion.

## Registers

### Substitute plugin

```diff
- The substitute plugin let’s us to just that.
+ The substitute plugin lets us to just that.
```

## Windows & tabs

```diff
- it will open a newindow
+ it will open a new window
```

`:e!` to refresh a buffer, if the corresponding file was updated and save elsewhere (eg. a git operation)

### LSP

```diff
- LSP is Neovim is a beast
+ LSP in Neovim is a beast
```

> brew install lua-language-server

Not everyone may use MacOS. How about linking the `lua-language-server` on GitHub so that people can check on the project wiki how to install it, based on their OS?

> We’ll have to install prettier

You forgot to introduce what prettier is about. One sentence should suffice,
like you did with other plugins.

### Fugitive plugin

You added some config around `:Gclog`, but you didn't mention it in your command
table underneath (with the other fugitive commands). It's a pretty powerful
feature of fugitive, so it may be worth mentioning it in that table.

(side note: I found out recently that `:Gclog` also pulls history from deleted
files. In vim, open a previously deleted file (`:e path/to/deleted_file`), you
of course end up with an empty buffer. Then when you type `:Gclog`, it still
pulls the deleted file's history. Pretty cool!)

--------

A few more thoughts:

- This is such a valuable resource!! Thanks for putting all of this together!
- I feel like there are some topics that are more advanced than others ("Basic
  configuration" vs LSP setup). Since your book is about vim for beginners and
  pros, what do you think about creating a related color code for each main section? Something like:
  - Green: beginner
  - Yellow: intermediate
  - Red: pro
- Maybe mention somewhere that the reader will be able to build up their config
  over time, over using vim extensively. They'll notice some repeated actions in
  their workflow, and will realize that they'll be better off by adding a new
  mapping or search for a plugin, etc. In short, to iterate over a given
  configuration as vim is used over a long period of time could be a good tip
  to give the readers.
